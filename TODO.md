* The `logparam` module is useful by itself to read Monte Python
  `log.param` files. Add documentation about how it can be imported
  and used.

* As a possible future development, instead of defining experiments
  under a sub-package, it may be better to allow users to import an
  `add_derived` method within a Python code. This function would
  accept an existing chain and a class defining the derived parameter
  computation as input parameters. The package should provide a
  generic class that can be inherited, hiding implementation details
  now exposed to the users. This will have two advantages:

  - No new installation required for new derived parameters
    computations.

  - Users no exposed to internal implementation details.

  This modification can be implemented in parallel to the present
  usage through the command-line. Indeed, general-purpose computations
  can be already delivered through command-line.
