*add_derived - add derived parameters to Monte Python chains.*

Copyright 2016-2017 Francesco Montanari.

Licensed under the GPL3+, see the COPYING file for more details.


Description
===========

Add derived parameters to existing Markov chain Monte Carlo (MCMC)
chains formatted according to [Monte
Python](https://baudren.github.io/montepython.html).

Monte Python already provides a module `montepython/add_derived.py` to
add derived parameters to existing chains. This package offers an
alternative implementation for optimized computations. For instance,
`montepython/add_derived.py` iterates the chains point by point,
whereas using Python libraries for arrays manipulation such as Numpy
often speeds up considerably the calculation.


Installation
============

The package depends on *Numpy*. Individual experiment modules may
require further dependencies, such as *Scipy* or specific versions of
*Monte Python* (e.g., [SAMP](http://fmnt.info/projects/)).

The package can be installed as:

```shell
$ python setup.py build
$ python setup.py install --user --record files.txt
```

The optional `--user` specifies that the installation will be placed
under the `~/.local` directory. The optional `--record files.txt`
option is useful to keep track of the installed files, to easily
remove them later:

```shell
$ cat files.txt | xargs rm -rf
```


Usage
=====

List all the available experiment modules to compute derived
parameters:

```shell
$ add_derived list
```

Compute derived parameters into an extended chain:

```shell
$ add_derived run EXPERIMENT DIR_CHAIN DIR_DERIVED
```

> The current version only extends chains implemented with the
> [SAMP](http://fmnt.info/projects/) version of Monte Python.

Help messages:

```shell
$ add_derived -h
$ add_derived list -h
$ add_derived run -h
```


Add new experiments
-------------------

Derived parameters must be computed under a dedicated experiment
folder. The logic is similar to the Monte Python likelihoods
computation.

*Copy and examine existing experiment modules to create new ones.*

Suppose we want to add an experiment called `MyExp`. Create a
`add_derived/experiment/MyExp/` folder. Under this folder, create an
empty `__init__.py` file, and a module with the same name as the
folder, `MyExp.py`. This module must contain a class with the same
name. Copy and adapt the modules from existing experiments. The class
should inherit from the `BaseExperiment` base class and implement at
least the following methods:

```python
from ...base_experiment import BaseExperiment

class MyExp(BaseExperiment):
    def __init__(self, *args):
        # Initialize the base class.
        BaseExperiment.__init__(self, *args)

        # List here the new derived parameters. For instance:
        # self.new_derived = ['deriv_p1', 'deriv_p2', ...]
        self.new_derived = []

    def get_new_derived(self, chain):
        # Compute derived parameters and return a list with elements
        # corresponding to the new columns to be added to the chains.
```

The initialization method calls the base class initialization method
to extract the likelihood information from the existing Monte Python
chain. All the likelihood parameters are then available for the
computation derived parameters. Define here the new derived
parameters.

The computation of derived parameters is performed within the method
`get_new_derived()`, which returns the new chain columns corresponding
to the new derived parameters.

For more information see the documentation of `base_experiment`
module:

```python
>>> import add_derived.base_experiment
>>> help(add_derived.base_experiment)
```

While developing, instead of istalling the code after each modification it is convenient to run it as:

```shell
$ python -m add_derived.add_derived *args*
```

(Running `python add_derived/add_derived.py` will rise `ValueError:
Attempted relative import in non-package`.)
