#!/usr/bin/env python

from setuptools import setup, find_packages
import add_derived

setup(name='add_derived',
      version=add_derived.__version__,
      description='Add derived parameters to Monte Python chains',
      classifiers=[
          "Development Status :: 4 - Beta",
          "Environment :: Console",
          "Intended Audience :: Developers",
          "Programming Language :: Python :: 2.7",
      ],
      author='Francesco Montanari',
      author_email='francesco.montanari@helsinki.fi',
      url='https://gitlab.com/cosmo-analysis/add_derived',
      license='GPL3+',
      packages=find_packages(),
      entry_points={'console_scripts':
                    ['add_derived = add_derived.add_derived:run',]},
)
