# Copyright (C) 2017 Francesco Montanari

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

"""Handle a Monte Python log.param file.

"""

import numpy as np
import os

class LogParam:
    """Read a log.param file from a Monte Python chain folder. Copy it
    into a new folder adding lines at the end of the file for new
    derived parameters.
    """


    def __init__(self,dir_chain):
        """Initialize the path to the existing log.param

        Parameters
        ----------
        dir_chain : str
            Path to the existing directory containing the log.param
            file.
        """
        self.dir_chain = dir_chain
        self.logtail = 'log.param'
        self.logpath = os.path.join(self.dir_chain, self.logtail)


    def read(self):
        """Read the original log.param file and return original data
        and likelihood parameters.

        Return
        ------
        lkl_experiments : list
            List of experiments names from the original chain.
        params : list
            List of the original chain parameters.
        lkl : dict
            Dictionary containing an entry for each original
            experiment. Each entry is a dictionary itself, containing
            all likelihood parameters for the given experiment. For
            example, lkl['exp1'] gives a dictionary containing all the
            parameters used to define the likelihood of the experiment
            'exp1'.
        """

        lkl_experiments = self._get_lkl_experiments()
        if not lkl_experiments:
            msg = "No experiment found in the original log.param file."
            raise RuntimeError(msg)
        nexp = len(lkl_experiments)

        params = []
        lkl = {}

        # Array containing chain parameters labels
        paramstr = 'data.parameters'
        for line in open(self.logpath, 'r'):
            lis = line.strip()
            if (lis.startswith(paramstr)):
                li = lis.split('=')
                elem = li[0].replace(paramstr,"")
                elem = elem.replace("['","")
                elem = elem.replace("']","")
                elem = elem.replace(" ","")
                params += [elem]

        # Dictionary containing dictionaries with the likelihood
        # parameters for each original experiment
        for paramstr in lkl_experiments:
            lkl[paramstr] = {}
            expdict = lkl[paramstr]
            for line in open(self.logpath, 'r'):
                lis = line.strip()
                if (lis.startswith(paramstr)):
                    li = lis.split('=')
                    elem = li[0].replace(paramstr+".","")
                    elem = elem.replace(" ","")
                    exec("expdict[elem] = %s" % li[1].replace(" ","") )

        return lkl_experiments, params, lkl


    def copyto(self, dir_derived, param_derived=[]):
        """Copy the original log.param file to the new folder, adding
        new derived parameters at the end of the file.

        Parameters
        ----------
        dir_derived : str
            Folder where the derived chains are saved.
        param_derived : list
            List of strings for the new derived parameters.
        """

        if not os.path.exists(dir_derived):
            os.makedirs(dir_derived)

        in_path = self.logpath
        out_path = os.path.join(dir_derived, self.logtail)
        with open(in_path, 'r') as input_log:
            with open(out_path, 'w') as output_log:
                # Read the whole file
                text = input_log.readlines()

                # Write everything up to this point to the output
                for index in range(len(text)):
                    output_log.write(text[index])

                section = ("\n\n#--------Additional-derived-parameters------\n")
                output_log.write(section)
                # Append lines for the new derived parameters
                for name in param_derived:
                    output_log.write( "data.parameters['%s'] = "
                                      "[0,-1,-1,0,1,'derived_nuisance']\n"
                                      % (name))

        print("Log parameter file created:\n --> %s" % out_path)


    def _get_lkl_experiments(self):
        """Read the name of the experiments from the original chain."""

        var = 'data.experiments'
        lkl_experiments = None

        for line in open(self.logpath, 'r'):
            lis = line.strip()
            if not lis.startswith("#"):
                li = lis.split('=')
                if li[0].strip() == var:
                    exec("lkl_experiments = %s" % (li[1]))

        return lkl_experiments
