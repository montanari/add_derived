# Copyright (C) 2017 Francesco Montanari

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

"""Parse command line input.

"""

import argparse
import os
import warnings


def get_version():
    from . import __version__

    return __version__


def read_cli():
    # Path of Monte Python main folder
    version = get_version()

    # Parse command line
    pmsg = "Add derived parameters to existing Monte Python chains."
    parser = argparse.ArgumentParser(description=pmsg)

    parser.add_argument('--version', action='version', version=version)

    subparsers = parser.add_subparsers()

    # Parser for the 'list' command
    lmsg = "list the available experiments"
    parser_l = subparsers.add_parser('list', help=lmsg)

    # Parser for the 'run' command
    rmsg = "compute derived parameters"
    parser_r = subparsers.add_parser('run', help=rmsg)

    emsg = "name of the experiment module"
    parser_r.add_argument('experiment', help=emsg, type=str)

    dcmsg = "path to the existing chain directory"
    parser_r.add_argument('dir_chain', help=dcmsg, type=str)

    ddmsg = "path to the output derived chain directory"
    parser_r.add_argument('dir_derived', help=ddmsg, type=str)

    # Parse the arguments
    args = parser.parse_args()

    return args
