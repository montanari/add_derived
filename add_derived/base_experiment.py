# Copyright (C) 2017 Francesco Montanari

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

"""Base experiment class.

"""

import numpy as np

class BaseExperiment():
    """Base experiment class. Concrete experiments inherit from this class
    and defined a customized `get_new_derived()` method.

    """

    def __init__(self, orig_exp, orig_param, orig_lkl, chains_dict,
                 dir_derived):
        """Initialize the info needed to compute the derived
        parameters. Inheriting classes should add new derived
        parameters names in the `self.new_derived` list.

        Parameters
        ----------
        orig_exp : str
            Folder containing the existing chains.
        orig_param : list
            List containing the names of the parameters in the
            original chains.
        orig_lkl : dict
            Dictionary with information about the original likelihood
            computation.
        chains_dict : dict
            Dictionary containing the original chains data.
        dir_derived : str
            Path of the derived chains.
        """

        # The following information about the original chains can be
        # used in the `get_new_derived()` function.
        self.orig_exp = orig_exp
        self.orig_param = orig_param
        self.orig_lkl = orig_lkl
        self.chains_dict = chains_dict
        self.derived_dict = {}
        self.dir_derived = dir_derived

        # List new derived parameters names in the `self.new_derived`
        # list.
        self.new_derived = None


    def get_new_derived(self, chain):
        """Compute the new derived parameters. Modify this method as
        needed in the inheriting class. It must return a list with the
        new columns to be added.

        Parameters
        ----------
        chain : list
            Original chain name.

        Return
        ------
        new_cols : list
            List with elements corresponding to the new columns to be
            added to the chains (one column for each derived
            parameter).

        """
        pass

    def get_new_derived_names(self):
        """Print the list of the new derived parameters. This method
        should always be present and probably need not to be modified.
        """
        return self.new_derived

    def _get_orig_params(self, chain):
        """Read the parameters values from the original chain and
        return a dictionary. This method should always be present and
        probably need not to be modified.
        """
        full_chain = self.chains_dict[chain]
        param = {}

        i = 0
        for elem in self.orig_param:
            # The first two columns are the multiplicity and -loglkl
            param[elem] = np.asarray(full_chain[:,2+i])
            i += 1

        return param
