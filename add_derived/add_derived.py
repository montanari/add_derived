# Copyright (C) 2017 Francesco Montanari

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

"""Main file.

"""

import pkgutil

import numpy as np

from . import chains
from . import experiment
from . import logparam
from . import parser

def run_main(module_derived, dir_chain, dir_derived):
    """
    Extract information from the original log.param and extend the
    original chains by adding new derived parameters.

    Parameters
    ----------
    module_derived : str
        Name of the module defined under the `add_derived/experiment`
        folder. This is where the new derived parameters are computed.
    dir_chain : str
        Existing directory containing the original chains.
    dir_derived : str
        New directory containing the chains extanded with the new
        derived parameters.
    """

    # Recover the original chains
    Chains = chains.Chains(dir_chain)
    chains_dict = Chains.get()

    # Recover the original likelihood parameters
    LogParam = logparam.LogParam(dir_chain)
    orig_exp, orig_param, orig_lkl = LogParam.read()

    args = (orig_exp, orig_param, orig_lkl, chains_dict, dir_derived)

    # Initialized the derived parameters module
    exec("from .experiment.{modname}.{modname} import {modname}"
         .format(modname=module_derived))
    exec("experiment = {}(*args)".format(module_derived))

    # Copy the log.param adding the new derived parameters
    param_derived = experiment.get_new_derived_names()
    LogParam.copyto(dir_derived, param_derived=param_derived)

    # Extend the chains
    new_cols_dict = {}
    for chain in chains_dict:
        print("Extending chain %s" % chain )
        new_cols = experiment.get_new_derived(chain)
        if len(new_cols) != len(param_derived):
            msg = ("The number of computed derived parameters does not\n"
                   "match the number of derived parameters\n"
                   "names. Check the `add_derived/experiment/` module.")
            raise RuntimeError(msg)
        # Put the new columns in a format suitable to be appended to a
        # table loaded with numpy.loadtxt
        new_cols = np.transpose(new_cols)
        new_cols_dict[chain] = new_cols

    Chains.extend(dir_derived, chains_dict, new_cols_dict)

def run_list():
    known_exps = [modname for __, modname, __ in
                 pkgutil.iter_modules(experiment.__path__)]

    print("Available derived experiments:")
    for modname in known_exps:
        print("    {}".format(modname))

def run():
    args = parser.read_cli()

    if 'experiment' in args:
        run_main(args.experiment,
                 args.dir_chain,
                 args.dir_derived)
    else:
        run_list()


if __name__ == '__main__':
    run()
