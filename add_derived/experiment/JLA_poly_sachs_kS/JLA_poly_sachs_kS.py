# Copyright (C) 2017 Francesco Montanari

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

"""This experiment computes the angular diameter distance sum rule
consistency condition, given polynomial fits to luminosity distance
and using Sachs' equation.

This module requires an existing installation of SAMP (see
<http://fmnt.info/projects/>), an adapted Monte Python.

"""

import os
import sys

import numpy as np
import montepython.polytools as pt

from ...base_experiment import BaseExperiment

class JLA_poly_sachs_kS(BaseExperiment):
    def __init__(self, *args):
        """Initialize the info needed to compute the derived parameters. List
        here the new derived parameters names in the
        `self.new_derived` list.

        """
        # Initialize the base class.
        BaseExperiment.__init__(self, *args)

        # List here the new derived parameters. Compute them in the
        # `get_new_derived()` function.
        self.new_derived = ['z0','Omega_m']

        self.nzl = 4 # Number of lensing redshifts
        self.nzs = 5 # Number of sources redshifts
        self.kSname = tuple('kSl'+str(i)+'s'+str(j) for i in range(self.nzl)
                                                    for j in range(self.nzs))
        self.new_derived += self.kSname

    def get_new_derived(self,chain):
        """Compute the new derived parameters. Modify this method as
        needed. Must return a list with the new columns to be added.

        Parameters
        ----------
        chain : list
            Original chain name.

        Return
        ------
        new_cols : list
            List with elements corresponding to the new columns to be
            added to the chains (one column for each derived
            parameter).
        """
        # Dictionary containing the original parameter values. Call
        # the corresponding column of values as `origin_param['param']`.
        orig_param_val = self._get_orig_params(chain)

        # Start adapting new experiment
        npoints = len(orig_param_val['c2'])
        c4 = np.zeros(npoints)
        c5 = np.zeros(npoints)
        for elem in self.orig_param:
            if elem == 'c2':
                c2 = orig_param_val[elem]
            elif elem == 'c3':
                c3 = orig_param_val[elem]
            elif elem == 'c4':
                c4 = orig_param_val[elem]
            elif elem == 'c5':
                c5 = orig_param_val[elem]

        # Recover the necessary original likelihood parameters (see
        # the log.param file for more information).
        orig_lkl = self.orig_lkl['JLA_poly']
        fitdist = orig_lkl['fit_distance']
        limits = orig_lkl['validation_range']

        # Redshift where to compute the derived functions.
        self._set_zlist(orig_lkl)

        # Compute derived parameters
        z0 = np.zeros(len(c2))
        Omega_m = np.zeros(len(c2))

        for kSn in self.kSname:
            exec("%s = np.zeros(len(c2))" % kSn)

        for i in range(len(c2)):
            coeff = (c2[i], c3[i], c4[i], c5[i])
            z0[i] = pt.sachs_z0(coeff, limits, fitdist)
            Omega_m[i] = pt.sachs_Omega_m(z0[i], coeff, fitdist)

            for l, zl in enumerate(self.zl):
                mask = (self.zlist[:,0] == zl)
                zlist = self.zlist[mask]
                zslist = self.zlist[mask][:,1]

                dl, ds_array, dls_array = pt.sachs_sumrule_d(zl,
                                                             zslist,
                                                             coeff,
                                                             Omega_m[i],
                                                             fitdist)
                kS = pt.kS_sachs(dl, ds_array, dls_array)

                for s, zs in enumerate(zslist):
                    name = "kSl"+str(l)+"s"+str(s)
                    exec("%s[i] = kS[s]" % name)

        # Create an array containing the new columns. In this
        # particular example we use the exec command to deal with an
        # arbitrary number of derived parameters.
        strout = '['
        for i, elem in enumerate(self.new_derived):
            strout += elem+','
        strout += ']'
        exec("new_cols = %s" % strout)

        return new_cols

    def _set_zlist(self,orig_lkl):
        """Redshift where to compute the Hubble and curvature parameters."""

        jla_mub = orig_lkl['data_directory']
        jla_mub = os.path.join(jla_mub,'jla_mub.txt')
        zjla = np.sort(np.loadtxt(jla_mub,usecols=(0,)))
        zmin = zjla[0]
        zmax = zjla[-1]

        self.zl = [0.1*(1. + 3.*i) for i in range(self.nzl)]
        if self.zl[0] < zmin:
            raise RuntimeError("zmin out of data bounds.")

        self.zs = []
        zlist = []
        for zl in self.zl:
            for zs in np.linspace(zmin+zl,zmax,self.nzs):
                self.zs += [zs]
                zlist += [(zl,zs)]

        self.zlist = np.asarray(zlist)

        if not os.path.exists(self.dir_derived):
            os.makedirs(self.dir_derived)
        out_z = os.path.join(self.dir_derived, 'zlist.dat')
        np.savetxt(out_z, self.zlist, header='z_lens z_source',
                   fmt='%g')
        print('--> '+out_z)
