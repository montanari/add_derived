# Copyright (C) 2017 Francesco Montanari

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

"""This experiment computes derived cosmological functions (assuming
Sachs' equation), based on polynomial fits to JLA and Hubble parameter
data.

This module requires an existing installation of SAMP (see
<http://fmnt.info/projects/>), an adapted Monte Python.

"""

import sys
import os

import montepython.polytools as pt
import numpy as np

from ...base_experiment import BaseExperiment

class JLA_hubble_poly(BaseExperiment):
    def __init__(self, *args):
        """Initialize the info needed to compute the derived parameters. List
        here the new derived parameters names in the
        `self.new_derived` list.

        """
        # Initialize the base class.
        BaseExperiment.__init__(self, *args)

        # List here the new derived parameters. Compute them in the
        # `get_new_derived()` function.
        self.new_derived = []
        self.nz = 10
        self.dLname = ['dL'+str(i) for i in range(self.nz)]
        self.new_derived += self.dLname
        self.hname = ['h'+str(i) for i in range(self.nz)]
        self.new_derived += self.hname
        self.kHname = ['kH'+str(i) for i in range(self.nz)]
        self.new_derived += self.kHname
        self.mediankHname = ['mediankH']
        self.new_derived += self.mediankHname

    def get_new_derived(self,chain):
        """Compute the new derived parameters. Modify this method as
        needed. Must return a list with the new columns to be added.

        Parameters
        ----------
        chain : list
            Original chain name.

        Return
        ------
        new_cols : list
            List with elements corresponding to the new columns to be
            added to the chains (one column for each derived
            parameter).
        """
        # Dictionary containing the original parameter values. Call
        # the corresponding column of values as `origin_param['param']`.
        orig_param_val = self._get_orig_params(chain)

        # Start adapting new experiment

        # The chain length can be read from any column.
        npoints = len(orig_param_val['c2'])

        # Read parameters form chains
        fitdist = self.orig_lkl['JLA_poly']['fit_distance']
        c2, c3, c4, c5 = self._get_jla_params(orig_param_val, npoints)
        p1, p2, p3 = self._get_hubble_params(orig_param_val, npoints)

        # Redshift where to compute the Hubble and curvature parameters.
        zlist = self._set_zlist()

        # Compute derived parameters
        for dLn in self.dLname:
            exec("%s = np.zeros(npoints)" % dLn)
        for hn in self.hname:
            exec("%s = np.zeros(npoints)" % hn)
        for kHn in self.kHname:
            exec("%s = np.zeros(npoints)" % kHn)
        for medkHn in self.mediankHname:
            exec("%s = np.zeros(npoints)" % medkHn)

        for i in range(npoints):
            coeff_jla = (c2[i], c3[i], c4[i], c5[i])
            dL, d1L, d2L = pt.dL_poly(zlist, coeff_jla, fitdist,
                                      deriv=True)
            d_co  = dL/(1. + zlist)
            d1_co = d1L/(1. + zlist) - np.power(1. + zlist, -2)*dL

            coeff_hub = (p1[i], p2[i], p3[i])
            hubble = pt.hubble_poly(zlist, coeff_hub)

            kH = (1. - np.power(hubble*d1_co, 2.)) / np.power(d_co, 2.)

            for j in range(self.nz):
                name = 'dL'+str(j)
                exec("%s[i] = dL[j]" % name)
                name = 'h'+str(j)
                exec("%s[i] = hubble[j]" % name)
                name = 'kH'+str(j)
                exec("%s[i] = kH[j]" % name)
            name = 'mediankH'
            exec("%s[i] = np.median(kH)" % name)

        # Create an array containing the new columns. In this
        # particular example we use the exec command to deal with an
        # arbitrary number of derived parameters.
        strout = '['
        for i, elem in enumerate(self.new_derived):
            strout += elem+','
        strout += ']'
        exec("new_cols = %s" % strout)

        return new_cols

    def _get_jla_params(self, orig_param_val, npoints):
        """Read JLA parameters from chains."""
        c4 = np.zeros(npoints)
        c5 = np.zeros(npoints)
        for elem in self.orig_param:
            if elem == 'c2':
                c2 = orig_param_val[elem]
            elif elem == 'c3':
                c3 = orig_param_val[elem]
            elif elem == 'c4':
                c4 = orig_param_val[elem]
            elif elem == 'c5':
                c5 = orig_param_val[elem]
        return (c2, c3, c4, c5)

    def _get_hubble_params(self, orig_param_val, npoints):
        """Read Hubble parameters from chains."""
        p3 = np.zeros(npoints)
        for elem in self.orig_param:
            if elem == 'p1':
                p1 = orig_param_val[elem]
            elif elem == 'p2':
                p2 = orig_param_val[elem]
            elif elem == 'p3':
                p3 = orig_param_val[elem]
        return (p1, p2, p3)

    def _set_zlist(self):
        """Redshift where to compute the Hubble and curvature parameters.
        """

        # Hubble redshifts.
        hubble_lkl = self.orig_lkl['hubble_poly']

        hubble = os.path.join(hubble_lkl['data_directory'],
                              hubble_lkl['data_file'])
        zh = np.sort(np.loadtxt(hubble, usecols=(0,)))
        zh_min = zh[0]
        zh_max = zh[-1]

        # JLA redshifts.
        jla_lkl = self.orig_lkl['JLA_poly']
        jla_mub = jla_lkl['data_directory']
        jla_mub = os.path.join(jla_mub,'jla_mub.txt')
        zjla = np.sort(np.loadtxt(jla_mub,usecols=(0,)))
        zj_min = zjla[0]
        zj_max = zjla[-1]

        # Set redshifts within the largest common interval.
        zlist = np.linspace(max(zj_min, zh_min),
                            min(zj_max, zh_max),
                            self.nz)

        if not os.path.exists(self.dir_derived):
            os.makedirs(self.dir_derived)
        out_z = os.path.join(self.dir_derived, 'zlist.dat')
        np.savetxt(out_z, zlist, header='Derived parameters redshifts')
        print('--> '+out_z)

        return zlist
