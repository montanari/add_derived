# Copyright (C) 2017 Francesco Montanari

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

"""This experiment computes the distance at required redshifts, based
on spline fits to JLA data.

This module requires an existing installation of SAMP (see
<http://fmnt.info/projects/>), an adapted Monte Python.

"""

import sys
import os

from montepython.cosmolib import cosmology as cc
import montepython.splinetools as st
import numpy as np
from scipy import interpolate
from scipy.misc import derivative

from ...base_experiment import BaseExperiment

class JLA_spline(BaseExperiment):
    def __init__(self, *args):
        """Initialize the info needed to compute the derived parameters. List
        here the new derived parameters names in the
        `self.new_derived` list.

        """
        # Initialize the base class.
        BaseExperiment.__init__(self, *args)

        # List here the new derived parameters. Compute them in the
        # `get_new_derived()` function.
        self.new_derived = []
        self.nz = 5
        self.dLname = ["dL"+str(i) for i in range(self.nz)]
        self.new_derived += self.dLname
        self.hname = ["d1L"+str(i) for i in range(self.nz)]
        self.new_derived += self.hname

        self.knots_dL = np.array(self.orig_lkl['JLA_spline']['knots'])
        self.dL_0 = 0. # Initial condition dL(z=0)=0
        self.d1L_0 = 1. # Initial condition dL'(z=0)=1

        # Redshift where to compute the Hubble and curvature parameters.
        self.zlist = self._set_zlist()

    def get_new_derived(self,chain):
        """Compute the new derived parameters. Modify this method as
        needed. Must return a list with the new columns to be added.

        Parameters
        ----------
        chain : list
            Original chain name.

        Return
        ------
        new_cols : list
            List with elements corresponding to the new columns to be
            added to the chains (one column for each derived
            parameter).
        """
        # Dictionary containing the original parameter values. Call
        # the corresponding column of values as `origin_param['param']`.
        orig_param_val = self._get_orig_params(chain)

        # Start adapting new experiment

        # The chain length can be read from any column.
        npoints = len(orig_param_val['d1_spl'])

        # Read parameters form chains
        # d1_spl, d2_spl, d3_spl, d4_spl = self._get_jla_params(orig_param_val,
        #                                                       npoints)
        d1_spl, d2_spl, d3_spl, d1L_f = self._get_jla_params(orig_param_val,
                                                      npoints)

        # Compute derived parameters
        for dLn in self.dLname:
            exec("%s = np.zeros(npoints)" % dLn)
        for dLpn in self.hname:
            exec("%s = np.zeros(npoints)" % dLpn)

        for i in range(npoints):
            # Spline the distance.
            # coeff_jla = (d1_spl[i], d2_spl[i], d3_spl[i], d4_spl[i])
            coeff_jla = (d1_spl[i], d2_spl[i], d3_spl[i])
            dL, d1L_kn, __ = st.spline_p0(self.zlist, self.knots_dL,
                                          coeff_jla, self.dL_0,
                                          deriv_0=(self.d1L_0, 1),
                                          deriv_f=(d1L_f[i], 1),
                                          want_y1=True)
            # d1L_kn is the distance at knots values. Interpolate it
            # at self.zlist values.
            d1L = self._get_d1L(self.zlist, self.knots_dL, d1L_kn)

            d_co  = dL/(1. + self.zlist)
            d1_co = d1L/(1. + self.zlist) - np.power(1. + self.zlist, -2.)*dL

            # d1_co = [derivative(self._d_co_at_z, z, dx=1.e-6,
            #                     args=(coeff_jla,)) for z in self.zlist]

            # LambdaCDM = cc.LambdaCDM()
            # mock = {'Omega_m': 0.3,
            #         'Omega_Lambda': 0.7,
            #         'h': 0.7}
            # # d_co = ((1. + self.zlist) * LambdaCDM.angular_distance(self.zlist)
            # #         / (2997.9/0.7))
            # # d1_co = 1. / LambdaCDM.efunc(self.zlist)

            for j in range(self.nz):
                name = "dL"+str(j)
                exec("%s[i] = dL[j]" % name)
                name = "d1L"+str(j)
                exec("%s[i] = d1L[j]" % name)

        # Create an array containing the new columns. In this
        # particular example we use the exec command to deal with an
        # arbitrary number of derived parameters.
        strout = '['
        for i, elem in enumerate(self.new_derived):
            strout += elem+','
        strout += ']'
        exec("new_cols = %s" % strout)

        return new_cols

    def _get_jla_params(self, orig_param_val, npoints):
        """Read JLA parameters from chains."""
        d1_spl = np.zeros(npoints)
        d2_spl = np.zeros(npoints)
        d3_spl = np.zeros(npoints)
        d4_spl = np.zeros(npoints)
        for elem in self.orig_param:
            if elem == 'd1_spl':
                d1_spl = orig_param_val[elem]
            elif elem == 'd2_spl':
                d2_spl = orig_param_val[elem]
            elif elem == 'd3_spl':
                d3_spl = orig_param_val[elem]
            elif elem == 'd4_spl':
                d4_spl = orig_param_val[elem]
            elif elem == 'y1_f':
                y1_f = orig_param_val[elem]
        # return (d1_spl, d2_spl, d3_spl, d4_spl)
        return (d1_spl, d2_spl, d3_spl, y1_f)

    def _get_d1L(self, zlist, knots_dL, d1L_kn):
        """Interpolate the distance derivative array given at some knot
        values. Return its interpolated value at self.zlist redshifts.
        """
        # In principle the first derivative of a cubic spline is
        # quadratic. However, to interpolate it with a quadratic
        # spline we would need to specify arbitrarily the boundary
        # conditions. Instead, we use linear interpolation which is
        # good enough for our purposes.
        d1L_interp = interpolate.interp1d(knots_dL, d1L_kn,
                                          kind='linear')
        d1L = np.array([d1L_interp(z) for z in zlist])

        return d1L

    def _d_co_at_z(self, redshift, coeff_jla):
        """Compute the comoving distance at a single redshift"""
        dL, __ = st.spline_p0([redshift], self.knots_dL, coeff_jla,
                              self.dL_0, deriv_0=(self.d1L_0, 1))
        return dL[0]/(1.+redshift)

    def _set_zlist(self):
        """Redshift where to compute the Hubble and curvature parameters.
        """

        # JLA redshifts.
        jla_lkl = self.orig_lkl['JLA_spline']
        jla_mub = jla_lkl['data_directory']
        jla_mub = os.path.join(jla_mub,'jla_mub.txt')
        zjla = np.sort(np.loadtxt(jla_mub,usecols=(0,)))
        zj_min = zjla[0]
        zj_max = zjla[-1]

        # Set redshifts within the largest common interval. Use a
        # slightest narrow interval to avoid problems with splines
        # range due to accuracy.
        zlist = np.linspace(1.01*zj_min,
                            0.99*zj_max,
                            self.nz)


        if not os.path.exists(self.dir_derived):
            os.makedirs(self.dir_derived)
        out_z = os.path.join(self.dir_derived, 'zlist.dat')
        np.savetxt(out_z, zlist, header='Derived parameters redshifts')
        print('--> '+out_z)

        return zlist
