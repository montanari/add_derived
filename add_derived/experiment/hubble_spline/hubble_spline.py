# Copyright (C) 2017 Francesco Montanari

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

"""This experiment computes the hubble parameter at required
redshifts, based on the spline fit to the Hubble parameter.

This module requires an existing installation of SAMP (see
<http://fmnt.info/projects/>), an adapted Monte Python.

"""

from collections import OrderedDict
import sys
import os

import montepython.splinetools as st
import numpy as np

from ...base_experiment import BaseExperiment

class hubble_spline(BaseExperiment):
    def __init__(self, *args):
        """Initialize the info needed to compute the derived parameters. List
        here the new derived parameters names in the
        `self.new_derived` list.

        """
        # Initialize the base class.
        BaseExperiment.__init__(self, *args)

        # List here the new derived parameters. Compute them in the
        # `get_new_derived()` function.
        self.new_derived = []
        self.nz = 5
        self.hname = ['h'+str(i) for i in range(self.nz)]
        self.new_derived += self.hname
        # h'(0)
        self.new_derived += ['hprime_zero']

        self.h_0 = 1.

    def get_new_derived(self,chain):
        """Compute the new derived parameters. Modify this method as
        needed. Must return a list with the new columns to be added.

        Parameters
        ----------
        chain : list
            Original chain name.

        Return
        ------
        new_cols : list
            List with elements corresponding to the new columns to be
            added to the chains (one column for each derived
            parameter).
        """
        # Dictionary containing the original parameter values. Call
        # the corresponding column of values as `origin_param['param']`.
        orig_param_val = self._get_orig_params(chain)

        # start adapting new experiment

        # Recover the necessary original likelihood parameters (see
        # the log.param file for more information).
        orig_lkl = self.orig_lkl['hubble_spline']

        # Read spline ordinates values from the chains.
        knots, ychain = self._get_chain_splines(orig_param_val, orig_lkl)
        npoints = len(ychain.values()[0])

        # Redshift where to compute the Hubble and curvature parameters.
        zlist = self._set_zlist(orig_lkl)

        for hn in self.hname:
            exec("%s = np.zeros(npoints)" % hn)
        hprime_zero = np.zeros(npoints)

        for i in range(npoints):
            y = np.asarray([ychain.values()[j][i]
                            for j in range(len(knots))])
            y_0 = 1.
            hubble, __ = st.spline_p0(zlist, knots, y, y_0)

            hprime_zero[i] = self._get_hprime_zero(knots, y)

            for j in range(self.nz):
                name = 'h'+str(j)
                exec("%s[i] = hubble[j]" % name)

        # Create an array containing the new columns. In this
        # particular example we use the exec command to deal with an
        # arbitrary number of derived parameters.
        strout = '['
        for i, elem in enumerate(self.new_derived):
            strout += elem+','
        strout += ']'
        exec("new_cols = %s" % strout)

        return new_cols

    def _get_chain_splines(self, orig_param_val, orig_lkl):
        """Return spline knots and a dictionary with the chain values
        of splines ordinates. We assume that alphabetical order of
        parameter names corresponds to increasing redshift knots.
        """
        ysplines = {}
        for param in self.orig_param:
            if param.endswith("_spl"):
                ysplines[param] = orig_param_val[param]

        # Sort dictionary by key.
        ychain = OrderedDict(sorted(ysplines.items(), key=lambda t: t[0]))

        ny = len(ychain.values())

        knots = orig_lkl['knots']
        if len(knots) != ny:
            raise RuntimeError('Number of knots inconsistent with '
                               'number of spline ordinates.')

        return knots, ychain

    def _get_hprime_zero(self, knots, coeff_hub):
        """Compute h'(0) as a numerical derivative."""
        eps = 1e-5
        heps, __ = st.spline_p0([eps], knots, coeff_hub,
                                self.h_0)
        hprime_zero = (heps[0] - self.h_0) / eps
        return hprime_zero

    def _set_zlist(self,orig_lkl):
        """Redshift where to compute the Hubble and curvature parameters."""

        data = os.path.join(orig_lkl['data_directory'],
                            orig_lkl['data_file'])
        z = np.sort(np.loadtxt(data, usecols=(0,)))
        zmin = z[0]
        zmax = z[-1]
        zlist = np.linspace(zmin,zmax,self.nz)

        if not os.path.exists(self.dir_derived):
            os.makedirs(self.dir_derived)
        out_z = os.path.join(self.dir_derived, 'zlist.dat')
        np.savetxt(out_z, zlist, header='Derived parameters redshifts')
        print('--> '+out_z)

        return zlist
