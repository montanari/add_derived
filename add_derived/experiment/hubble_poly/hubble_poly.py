# Copyright (C) 2017 Francesco Montanari

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

"""This experiment computes the hubble parameter at required
redshifts, based on polynomial fit to the Hubble parameter.

This module requires an existing installation of SAMP (see
<http://fmnt.info/projects/>), an adapted Monte Python.

"""

import sys
import os

import montepython.polytools as pt
import numpy as np

from ...base_experiment import BaseExperiment

class hubble_poly(BaseExperiment):
    def __init__(self, *args):
        """Initialize the info needed to compute the derived parameters. List
        here the new derived parameters names in the
        `self.new_derived` list.

        """
        # Initialize the base class.
        BaseExperiment.__init__(self, *args)

        # List here the new derived parameters. Compute them in the
        # `get_new_derived()` function.
        self.new_derived = []
        self.nz = 5
        self.hname = ['h'+str(i) for i in range(self.nz)]
        self.new_derived += self.hname
        # h'(0)
        self.new_derived += ['hprime_zero']

    def get_new_derived(self,chain):
        """Compute the new derived parameters. Modify this method as
        needed. Must return a list with the new columns to be added.

        Parameters
        ----------
        chain : list
            Original chain name.

        Return
        ------
        new_cols : list
            List with elements corresponding to the new columns to be
            added to the chains (one column for each derived
            parameter).
        """
        # Dictionary containing the original parameter values. Call
        # the corresponding column of values as `origin_param['param']`.
        orig_param_val = self._get_orig_params(chain)

        # Start adapting new experiment
        npoints = len(orig_param_val['p1'])
        p3 = np.zeros(npoints)
        for elem in self.orig_param:
            if elem == 'p1':
                p1 = orig_param_val[elem]
            elif elem == 'p2':
                p2 = orig_param_val[elem]
            elif elem == 'p3':
                p3 = orig_param_val[elem]

        # Recover the necessary original likelihood parameters (see
        # the log.param file for more information).
        orig_lkl = self.orig_lkl['hubble_poly']

        # Redshift where to compute the Hubble and curvature parameters.
        zlist = self._set_zlist(orig_lkl)

        for hn in self.hname:
            exec("%s = np.zeros(len(p1))" % hn)
        hprime_zero = np.zeros(npoints)

        for i in range(len(p1)):
            coeff = (p1[i], p2[i], p3[i])
            hubble = pt.hubble_poly(zlist, coeff)

            for j in range(self.nz):
                name = 'h'+str(j)
                exec("%s[i] = hubble[j]" % name)

            hprime_zero[i] = p1[i]

        # Create an array containing the new columns. In this
        # particular example we use the exec command to deal with an
        # arbitrary number of derived parameters.
        strout = '['
        for i, elem in enumerate(self.new_derived):
            strout += elem+','
        strout += ']'
        exec("new_cols = %s" % strout)

        return new_cols

    def _set_zlist(self,orig_lkl):
        """Redshift where to compute the Hubble and curvature parameters."""

        data = os.path.join(orig_lkl['data_directory'],
                            orig_lkl['data_file'])
        z = np.sort(np.loadtxt(data, usecols=(0,)))
        zmin = z[0]
        zmax = z[-1]
        zlist = np.linspace(zmin,zmax,self.nz)

        if not os.path.exists(self.dir_derived):
            os.makedirs(self.dir_derived)
        out_z = os.path.join(self.dir_derived, 'zlist.dat')
        np.savetxt(out_z, zlist, header='Derived parameters redshifts')
        print('--> '+out_z)

        return zlist
