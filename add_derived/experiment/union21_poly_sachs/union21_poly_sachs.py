# Copyright (C) 2017 Francesco Montanari

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

"""This experiment computes derived cosmological functions (assuming
Sachs' equation), based on polynomial fits to Union 2.1 data.

This module requires an existing installation of SAMP (see
<http://fmnt.info/projects/>), an adapted Monte Python.

"""

import sys
import os

import montepython.polytools as pt
import numpy as np

from ...base_experiment import BaseExperiment

class union21_poly_sachs(BaseExperiment):
    def __init__(self, *args):
        """Initialize the info needed to compute the derived parameters. List
        here the new derived parameters names in the
        `self.new_derived` list.

        """
        # Initialize the base class.
        BaseExperiment.__init__(self, *args)

        # List here the new derived parameters. Compute them in the
        # `get_new_derived()` function.
        self.new_derived = ['z0', 'Omega_m']

        self.nz = 5

        self.dLname = ['dL'+str(i) for i in range(self.nz)]
        self.new_derived += self.dLname
        self.hname = ['h'+str(i) for i in range(self.nz)]
        self.new_derived += self.hname
        self.kHname = ['kH'+str(i) for i in range(self.nz)]
        self.new_derived += self.kHname


    def get_new_derived(self,chain):
        """Compute the new derived parameters. Modify this method as
        needed. Must return a list with the new columns to be added.

        Parameters
        ----------
        chain : list
            Original chain name.

        Return
        ------
        new_cols : list
            List with elements corresponding to the new columns to be
            added to the chains (one column for each derived
            parameter).
        """
        # Dictionary containing the original parameter values. Call
        # the corresponding column of values as `origin_param['param']`.
        orig_param_val = self._get_orig_params(chain)

        # Start adapting new experiment
        npoints = len(orig_param_val['c2'])
        c4 = np.zeros(npoints)
        c5 = np.zeros(npoints)
        for elem in self.orig_param:
            if elem == 'c2':
                c2 = orig_param_val[elem]
            elif elem == 'c3':
                c3 = orig_param_val[elem]
            elif elem == 'c4':
                c4 = orig_param_val[elem]
            elif elem == 'c5':
                c5 = orig_param_val[elem]

        # Recover the necessary original likelihood parameters (see
        # the log.param file for more information).
        orig_lkl = self.orig_lkl['union21_poly']
        fitdist = orig_lkl['fit_distance']
        limits = orig_lkl['validation_range']

        # Redshift where to compute the Hubble and curvature parameters.
        zlist = self._set_zlist(orig_lkl)

        # Compute derived parameters
        z0 = np.zeros(len(c2))
        Omega_m = np.zeros(len(c2))

        for dLn in self.dLname:
            exec("%s = np.zeros(len(c2))" % dLn)
        for hn in self.hname:
            exec("%s = np.zeros(len(c2))" % hn)
        for kHn in self.kHname:
            exec("%s = np.zeros(len(c2))" % kHn)

        for i in range(len(c2)):
            coeff = (c2[i], c3[i], c4[i], c5[i])
            z0[i] = pt.sachs_z0(coeff, limits, fitdist)
            Omega_m[i] = pt.sachs_Omega_m(z0[i], coeff, fitdist)

            hubble = pt.sachs_hubble(zlist, coeff, Omega_m[i], fitdist)

            dL, d1L, d2L = pt.dL_poly(zlist, coeff,
                                      fitdist, deriv=True)
            d_co  = dL/(1.+zlist)
            d1_co = d1L/(1.+zlist) - np.power(1.+zlist,-2)*dL
            kH = (1 - np.power(hubble*d1_co,2.)) / np.power(d_co,2.)

            for j in range(self.nz):
                name = 'dL'+str(j)
                exec("%s[i] = dL[j]" % name)
                name = 'h'+str(j)
                exec("%s[i] = hubble[j]" % name)
                name = 'kH'+str(j)
                exec("%s[i] = kH[j]" % name)

        strout = '['
        for i, elem in enumerate(self.new_derived):
            strout += elem+','
        strout += ']'
        exec("new_cols = %s" % strout)

        return new_cols

    def _set_zlist(self,orig_lkl):
        table = orig_lkl['data_directory']
        table = os.path.join(table,'sn_z_mu_dmu_union2.1.txt')
        zjla = np.sort(np.loadtxt(table,usecols=(1,)))
        zmin = zjla[0]
        zmax = zjla[-1]
        zlist = np.linspace(zmin,zmax,self.nz)

        zmin = 0.015
        zmax = 1.414
        zlist = np.linspace(zmin,zmax,self.nz)

        if not os.path.exists(self.dir_derived):
            os.makedirs(self.dir_derived)
        out_z = os.path.join(self.dir_derived, 'zlist.dat')
        np.savetxt(out_z, zlist, header='Derived parameters redshifts')
        print('Copy this file to the derived chain directory: %s' %out_z)

        return zlist

    # def _is_consistent(self, orig_param, chains_dict):
    #     nparam_log = len(orig_param)
    #     for fname in chains_dict:
    #         chain = chains_dict[fname]
    #         nparam_chain = len(chain[0])-2
    #         if nparam_log != nparam_chain:
    #             msg = ("The number of parameters in the chain is not "
    #                    "consistent with the number of parameters in "
    #                    "the `log.param` file.")
    #             raise RuntimeError(msg)
