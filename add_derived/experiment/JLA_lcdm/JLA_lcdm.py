# Copyright (C) 2017 Francesco Montanari

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

"""This experiment computes the luminosity distance at required
redshifts, based on LCDM fits to JLA data.

This module requires an existing installation of SAMP (see
<http://fmnt.info/projects/>), an adapted Monte Python.

"""

import sys
import os

from montepython.cosmolib import cosmology as cc
import numpy as np

from ...base_experiment import BaseExperiment

class JLA_lcdm(BaseExperiment):
    def __init__(self, *args):
        """Initialize the info needed to compute the derived parameters. List
        here the new derived parameters names in the
        `self.new_derived` list.

        """
        # Initialize the base class.
        BaseExperiment.__init__(self, *args)

        # List here the new derived parameters. Compute them in the
        # `get_new_derived()` function.
        self.new_derived = []
        self.nz = 5
        self.dLname = ['dL'+str(i) for i in range(self.nz)]
        self.new_derived += self.dLname


    def get_new_derived(self, chain):
        """Compute the new derived parameters. Modify this method as
        needed. Must return a list with the new columns to be added.

        Parameters
        ----------
        chain : list
            Original chain name.

        Return
        ------
        new_cols : list
            List with elements corresponding to the new columns to be
            added to the chains (one column for each derived
            parameter).
        """
        # Dictionary containing the original parameter values. Call
        # the corresponding column of values as `origin_param['param']`.
        orig_param_val = self._get_orig_params(chain)

        # Start adapting new experiment

        # The chain length can be read from any column.
        npoints = len(orig_param_val['Omega_m'])

        # Read parameters form chains
        Omega_m, Omega_k = self._get_common_params(orig_param_val, npoints)

        # Redshift where to compute the distance.
        zlist = self._set_zlist()

        # Compute derived parameters
        for dLn in self.dLname:
            exec("%s = np.zeros(npoints)" % dLn)

        for i in range(npoints):
            d_co = self._get_d(zlist, Omega_m[i], Omega_k[i])
            dL = (1. + zlist) * d_co

            for j in range(self.nz):
                name = 'dL'+str(j)
                exec("%s[i] = dL[j]" % name)

        # Create an array containing the new columns. In this
        # particular example we use the exec command to deal with an
        # arbitrary number of derived parameters.
        strout = '['
        for i, elem in enumerate(self.new_derived):
            strout += elem+','
        strout += ']'
        exec("new_cols = %s" % strout)

        return new_cols

    def _get_common_params(self, orig_param_val, npoints):
        """Read JLA parameters from chains."""
        for elem in self.orig_param:
            if elem == 'Omega_m':
                Omega_m = orig_param_val[elem]
            elif elem == 'Omega_k':
                Omega_k = orig_param_val[elem]
        return (Omega_m, Omega_k)

    def _set_zlist(self):
        """Redshift where to compute the distance.
        """
        # JLA redshifts.
        jla_lkl = self.orig_lkl['JLA_lcdm']
        jla_mub = jla_lkl['data_directory']
        jla_mub = os.path.join(jla_mub,'jla_mub.txt')
        zjla = np.sort(np.loadtxt(jla_mub,usecols=(0,)))
        zj_min = zjla[0]
        zj_max = zjla[-1]

        zlist = np.linspace(zj_min, zj_max, self.nz)

        if not os.path.exists(self.dir_derived):
            os.makedirs(self.dir_derived)
        out_z = os.path.join(self.dir_derived, 'zlist.dat')
        np.savetxt(out_z, zlist, header='Derived parameters redshifts')
        print('--> '+out_z)

        return zlist

    def _get_d(self, zlist, Omega_m, Omega_k):
        """Return reduced comoving distance d_co = (1+z)*H0*D_A (where
        D_A is the angular diameter distance), for LCDM.
        """
        # The result will not depend on this parameter. Need to set it
        # just to call correctly the cosmological module.
        h = 0.7
        H0_iMpc = h / 2997.9

        LambdaCDM = cc.LambdaCDM()
        cosmo = {'h' : h,
                 'Omega_m' : Omega_m,
                 'Omega_k' : Omega_k}
        LambdaCDM.set_cosmo(cosmo)

        # d_A = H0*D_A, does not depend on the choice made for h.
        dA = LambdaCDM.angular_distance(zlist) * H0_iMpc

        d_co = (1. + zlist) * dA

        return d_co
