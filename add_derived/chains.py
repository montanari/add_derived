# Copyright (C) 2017 Francesco Montanari

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

"""Handle MCMC chains formatted as in Monte Python.

"""

import numpy as np
import os

class Chains:
    def __init__(self,dir_chain):
        """
        Parameters
        ----------
        dir_chain : str
            Existing folder containing the chains.
        """
        self.dir_chain = dir_chain


    def get(self):
        """
        Recover the existing chains. Return a dictionary containing
        the chain file and the respective data.
        """
        folder, files, basename = self._recover_folder_and_files(
            [self.dir_chain])

        chains_dict = {}

        for fname in files:
            chain = np.loadtxt(fname, comments='#')
            if len(chain[0]) < 3:
                msg = ("The chains should contain at least 3 columns:\n"
                       "multiplicity, -loglkl and one parameter.")
                raise RuntimeError(msg)
            fhead, ftail = os.path.split(fname)
            exec("chains_dict['%s'] = chain" % (ftail))

        return chains_dict


    def extend(self, dir_derived, chains_dict, new_cols_dict):
        """
        Parameters
        ----------
        dir_derived : str
            Directory where the extended chains will be saved.
        chains_dict : dict
            Dictionary containing the chains data. Each chains must be
            a numpy array with shape (l,m), where l is the number of
            points in parameter space, and m is the number of columns
            of the chain.
        new_cols_dict : dict
            Dictionary containing the new derived parameters
            data. Each data must by a numpy array with shape (l,n),
            where l is the number of points in parameter space, and n
            is the number of the new derived parameters.
        """

        nchain = len(chains_dict)
        nnew = len(new_cols_dict)
        if nchain != nnew:
            msg = ("The number (%s) of original chains does not match the "
                   "number %(s) of derived chains" % (nchai,nnew))
            raise RuntimeError(msg)

        for fname in chains_dict:
            chain = chains_dict[fname]
            cols = new_cols_dict[fname]

            if len(chain) != len(cols):
                msg = ("The new columns length does not match the "
                       "original chain length. Check also that the "
                       "shape of the data is consistent (hint: try to "
                       "transpose the new columns table).")
                raise RuntimeError(msg)

            new_chain = np.append(chain, cols, 1)

            fout = os.path.join(dir_derived,fname)
            np.savetxt(fout, new_chain)
            print('--> '+fout)

    def _recover_folder_and_files(self,files):
        """
        Distinguish the cases when analyze is called with files or folder.

        Note that this takes place chronologically after the function
        `separate_files`.

        This method is adapted from Monte Python.
        """
        # The following list defines the substring that a chain should
        # contain for the code to recognise it as a proper chain.
        substrings = ['.txt', '__']
        limit = 10

        # If the first element is a folder, grab all chain files inside
        if os.path.isdir(files[0]):
            folder = os.path.normpath(files[0])
            files = [os.path.join(folder, elem) for elem in os.listdir(folder)
                     if not os.path.isdir(os.path.join(folder, elem))
                     and not os.path.getsize(os.path.join(folder, elem)) < limit
                     and all([x in elem for x in substrings])]
        # Otherwise, extract the folder from the chain file-name.
        else:
            # If the name is completely wrong, say it
            if not os.path.exists(files[0]):
                raise IOError(
                    "You provided a non-existent folder/file to analyze")
            folder = os.path.relpath(
                os.path.dirname(os.path.realpath(files[0])), os.path.curdir)
            files = [os.path.join(folder, elem) for elem in os.listdir(folder)
                     if os.path.join(folder, elem) in np.copy(files)
                     and not os.path.isdir(os.path.join(folder, elem))
                     and not os.path.getsize(os.path.join(folder, elem)) < limit
                     and all([x in elem for x in substrings])]
        basename = os.path.basename(folder)

        return folder, files, basename
